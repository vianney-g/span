from __future__ import unicode_literals
from docutils.parsers.rst import roles
from docutils import nodes, utils
from pelican.readers import PelicanHTMLTranslator

class span(nodes.Inline, nodes.Node):
    # Subclassing Node directly since TextElement automatically appends the escaped element
    def __init__(self, rawsource, text):
        self.rawsource = rawsource
        self.text = text
        self.children = []
        self.attributes = {}

    def astext(self):
        return self.text

def entity_role(typ, rawtext, text, lineno, inliner, options={}, content=[]):
    text = utils.unescape(text)
    html_span = '<span class="{1}">{0}</span>'.format(*text.split('|'))
    return [span(text, html_span)], []

def register():
    roles.register_local_role('span', entity_role)

PelicanHTMLTranslator.visit_span = lambda self, node: self.body.append(node.astext())
PelicanHTMLTranslator.depart_span = lambda self, node: None
