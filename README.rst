Span for reStructuredText
##################################

This plugin allows you to insert span tags with classes in a RST document.
I needed this kind of stuff to add more control on the HTML output.
It is adapted from the html_entities plugin

Roles
=====

Add one inline role:

::

    :span:

Usage
=====

::

    :span:`Notre Dame de Paris|book-title another-class`

produces:

<span class="book-title another-class">Notre Dame de Paris</span>
